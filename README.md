# TPE

_Note: this repo was made for a french school project. As such, it is only available in french._

Dépot pour les Travaux Pratiques Encadrés (TPE) réalisés dans le cadre de l'année scolaire *2013/2014*.  
Date butoir : janvier 2014.

## Site

Contient les fichiers du site réalisé par ThibaudGAMARD et grandement amélioré par Matteo78.

[Voir le site](http://mattoufp.github.io/tpe)  
[Voir le code source](http://github.com/MattouFP/tpe/tree/gh-pages)

## Modèle

Contient les fichiers de la modélisation en trois dimensions réalisée par matteo78.

[Voir la branche](http://github.com/matteo78/tpe/tree/model)

## Dossier

Contient le document markdown du dossier devant être rendu imprimé à l'issue de la réalisation du TPE. Contient également les images utilisées pour rendre les graphiques et schémas.

[Voir le dossier](http://github.com/MattouFP/tpe/blob/master/tpe.pdf)

Ce dossier est constitué par l'ensemble des participants du TPE.

### Génération du PDF

Le dossier est initialement rédigé en langage LaTeX. Il peut être aisément converti à l'aide du script *compiler.bat*. Assurez-vous d'avoir installé [MiKTEX](http://miktex.org/download) au préalable.

Le dernier PDF est également disponible : *tpe.pdf.*