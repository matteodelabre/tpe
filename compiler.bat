@echo off

pdflatex -help >nul 2>&1 && (
	bibtex -help >nul 2>&1 && (
		set hasutils=1
	)
)

if defined hasutils (
	@title Compilation en cours...
	echo Compilation en cours...
	
	del tpe.aux
	del tpe.bbl
	del tpe.blg
	del tpe.brf
	del tpe.log
	del tpe.toc
	del tpe.tps
	del tpe.out
	
	pdflatex tpe.tex
	bibtex tpe
	pdflatex tpe.tex
	pdflatex tpe.tex
	
	del tpe.aux
	del tpe.bbl
	del tpe.blg
	del tpe.brf
	del tpe.log
	del tpe.toc
	del tpe.tps
	del tpe.out
	
	echo Compilation terminée !
	start tpe.pdf
) else (
	echo Veuillez installer MiKTeX.
	timeout 5
	start http://miktex.org/download
)