\section{Applications} 

La grande précision du système \textsc{Gps} et sa fiabilité ont permis son développement aussi bien dans notre quotidien que dans les recherches scientifiques. En effet, il se révèle très utile lorsque les études à échelle humaine ne s'avèrent plus suffisantes et que des coordonnées terrestres entrent en jeu. C'est le cas par exemple pour le suivi d'animaux, ou de plaques tectoniques.

Il s'agira ici de détailler les procédés utilisés pour ces suivis ainsi que les étapes dans lesquelles interviennent les balises. Nous développerons ainsi au travers de ces exemples différents moyens de traiter les données reçues depuis le satellite.

\subsection{Suivi des animaux par balise \sc{Gps}}

Le suivi des animaux permet de retracer leurs migrations et déplacements, et ainsi de mieux comprendre leur comportement. Le dispositif est simple en soi, puisqu'il s'agit d'attacher une balise \textsc{Gps} à un membre de l'animal, puis de récupérer à intervalle prédéfini (la plupart du temps toutes les heures ou tous les jours) sa position.

Nous nous sommes ici intéressés au suivi d'un loup, lors de l’été 1998. \`{A} partir de données en provenance d'Internet~\cite{edusTecani}, nous avons pu dresser une carte de ses déplacements présentée en figure~\ref{fig:applicationsloups1}.

L’analyse de cette carte nous permet de constater qu'il a parcouru près de \unit{169}{\kilo\meter} durant la période~---~soit un peu moins d'un kilomètre par jour, et qu'il revient fréquemment sur ses pas en se limitant à une zone restreinte.

On observe une grande mobilité du loup suivi, ce qui nous amène à penser qu'il s'agit d'un mâle, puisque ces derniers ont une plus grande tendance à chasser que les femelles. De plus, il adopte un comportement territorial~: il délimite et défend une zone de laquelle il ne sort que rarement. Ce territoire peut atteindre les \unit{300}{\kilo\meter\squared}~; il est délimité en urinant. Il faut néanmoins noter qu'il n’est pas fixe. En effet, il peut changer selon la saison, voire être définitivement abandonné~\cite{tendYrwolf}.

\begin{figure}
	\caption{Suivi journalier des déplacements d'un loup pendant 6 mois}
	\label{fig:applicationsloups1}
	\begin{center}
		\includegraphics [width=15cm]{applications/loups_fig1.png}
	\end{center}
\end{figure}

\clearpage

\subsection{Suivi des plaques}

Le modèle actuel de fonctionnement de la Terre décrit l'existence de plaques qui se déplacent à la surface du globe. Il est possible de mesurer ces déplacements à l'aide du système \textsc{Gps}. Il s'agit de placer des stations de mesure enregistrant régulièrement leur propre position. Chaque plaque possède plusieurs stations qui permettent d'affiner le résultat.

Nous avons étudié les rapports de position de 11~plaques tectoniques. Ils consistent en des séries de données de positionnement brut de la plaque sur la planète, desquelles il est possible d'extraire plusieurs informations. Dans la table~\ref{tab:pamadata} est présenté un échantillon des données de positionnement de la station \emph{PAMA}, que vous trouverez dans leur intégralité en annexe.

\begin{table}
	\caption{\'{E}chantillon des données brutes de positionnement de la station \emph{PAMA}}
	\label{tab:pamadata}
	\begin{center}
		\begin{tabular}{|c|c|c|}
			\hline
			Date de mesure & Latitude & Longitude\\
			\hline
			20/06/1992 & -30,6471 & 53,1861\\
			\hline
			21/06/1992 & -28,9186 & 51,6720\\
			\hline
			22/06/1992 & -27,9006 & 59,6851\\
			\hline
			23/06/1992 & -27,6100 & 55,0384\\
			\hline
			24/06/1992 & -28,5706 & 57,9379\\
			\hline
			25/06/1992 & -28,8176 & 59,0753\\
			\hline
			26/06/1992 & -29,0058 & 57,4589\\
			\hline
			30/06/1992 & -28,6421 & 59,7794\\
			\hline
			01/07/1992 & -27,5627 & 60,1351\\
			\hline
			07/07/1992 & -27,6240 & 58,0684\\
			\hline
			08/07/1992 & -29,0109 & 59,0861\\
			\hline
			09/07/1992 & -30,5866 & 55,7351\\
			\hline
			10/07/1992 & -29,3825 & 58,0039\\
			\hline
			11/07/1992 & -28,6344 & 58,6819\\
			\hline
			12/07/1992 & -29,3725 & 59,9400\\
			\hline
			13/07/1992 & -28,4454 & 56,1741\\
			\hline
			14/07/1992 & -29,5270 & 60,9324\\
			\hline
			15/07/1992 & -28,2739 & 60,2352\\
			\hline
			... & ... & ...\\
			\hline
			29/03/1997 & -14,3673 & 30,4584\\
			\hline
		\end{tabular}
	\end{center}
\end{table}

L'étude des données récupérées permet d'attester le déplacement des plaques. En effet, ces dernières se déplacent au rythme moyen de \unit{42,08}{\milli\meter\per{}an}. De plus, on constate la présence de zones de convergence, comme par exemple la subduction des plaques Philippines et nord-américaines par la plaque Pacifique~; de divergence, comme au niveau de la dorsale qui sépare la plaque nord-américaine des plaques eurasiennes et africaines~; et de décrochement, comme entre la plaque nord-américaine et la plaque Pacifique.~\cite{wikiTecpla}

Le déplacement moyen individuel des stations étudiées est présenté en table~\ref{tab:rawdataplatesmoving}.

\begin{table}
	\caption{\'{E}tude du déplacement de quelques stations}
	\label{tab:rawdataplatesmoving}
	\begin{center}
		\begin{tabular}{|c|c|c|c|}
			\hline
			\multirow{2}*{Nom} &
			\multicolumn{3}{c|}{Déplacement annuel (en \milli\meter\per{}an)}\\
			\cline{2-4}
			~ & Latitude & Longitude & Total\\
			\hline
			ASC1 & 8,81 & -6,49 & 10,94\\
			\hline
			EISL & -8,37 & 64,90 & 65,44\\
			\hline
			IISC & 31,51 & 42,51 & 52,91\\
			\hline
			MALI & 11,74 & 27,00 & 29,44\\
			\hline
			MDO1 & -9,87 & -11,72 & 15,32\\
			\hline
			MKEA & 30,44 & -63,34 & 70,27\\
			\hline
			PAMA & 28,22 & -62,13 & 68,24\\
			\hline
			PERT & 55,75 & 42,02 & 69,81\\
			\hline
			SANT & 15,24 & 18,11 & 23,67\\
			\hline
			TAEJ & -19,61 & 28,13 & 34,29\\
			\hline
			YELL& -13,39 & -18,17 & 22,57\\
			\hline
		\end{tabular}
	\end{center}
\end{table}

Les vecteurs de déplacement constatés sur les stations étudiées sont présentés en figure~\ref{fig:applicationstp1}, réalisée à l'aide du logiciel GeoGebra.

\begin{figure}
	\caption{Répresentation des vecteurs de déplacement de quelques stations}
	\label{fig:applicationstp1}
	\begin{center}
		%\includegraphics[trim=20cm 6cm 30cm 4cm, clip=true, totalheight=0.6\textheight, angle=90]{applications/tp_fig1.pdf}
		\includegraphics[trim=3cm 1cm 2cm 1cm, clip=true, totalheight=0.55\textheight, angle=90]{applications/tp_fig1.pdf}
	\end{center}
\end{figure}