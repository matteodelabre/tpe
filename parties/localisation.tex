\section{Localisation d'un récepteur \sc{Gps}}

\subsection{Principe}

Le système \textsc{Gps} est réparti en segments. Deux d'entre eux sont le segment utilisateur --- les balises, et le segment spatial --- les satellites. Le dialogue entre ces deux parties est unidirectionnel, dans le sens où les satellites émettent en permanence des signaux qui sont ensuite captés par les récepteurs, qui ne renvoient pas d'informations au premier. Cette organisation permet au système de fonctionner sans interruption, quel que soit le nombre de personnes souhaitant connaître leur position, puisque le seul rôle du satellite est d'émettre les signaux~\cite{msuvCplgps}.

Chaque satellite émet en effet des ondes électromagnétiques sur deux fréquences \unit{1575,42}{\mega\hertz} (L1) et \unit{1227,60}{\mega\hertz} (L2). Ces signaux contiennent deux codes appelés P (pour \emph{Precise}, précis) et C/A (pour \emph{Coarse Acquisition}, acquisition grossière). Seul ce dernier est utilisable par les civils, le premier étant crypté et réservé à l'usage militaire américain.

La balise capte par la suite des signaux d'au moins quatre satellites qui lui permettent de résoudre une équation appelée\ \og~équation de navigation~\fg. Les suivants lui permettent de corriger d'éventuelles erreurs de mesure. Afin de déterminer cette équation, nous ne disposons que d'une information~: la distance actuelle entre le récepteur et un des satellites de la constellation.

Cette distance se détermine aisément, puisque nous savons que les ondes électromagnétiques se déplacent, dans l'espace, à la vitesse $c$ de la lumière et que nous connaissons son heure précise d'envoi $t_0$, ainsi que son heure d'arrivée $t_1$. Cela nous permet d'obtenir le temps $\Delta{}t$ qu'a mis cette onde pour se propager. En utilisant la relation physique suivante~:
\begin{dgroup*}
	\begin{dseries*}
		\begin{math}v = \frac{d}{\Delta{}t}\end{math}\condition{avec $v = c$ et $\Delta{}t = t_1 - t_0$}
	\end{dseries*}
	\begin{dmath*}d = v\cdot\Delta{}t\end{dmath*},
\end{dgroup*}
on obtient la distance $d$ du récepteur au satellite.

\begin{figure}
	\caption{\'{E}quation cartésienne du cercle}
	\label{fig:equations2}
	\begin{center}
		\includegraphics[width=15cm]{equations/fig2.pdf}
	\end{center}
\end{figure}

\parindent=0cm
\begin{minipage}[t]{0.5\linewidth}
	% apply indenting
	\parindent=\oldparindent
	\subsection{\'{E}quation de navigation}
	
	Résoudre l'équation de navigation consiste à trouver le point qui est à la fois à la distance $r_A$ d'un premier satellite $A$, à la distance $r_B$ d'un second satellite $B$ et à la distance $r_C$ d'un dernier satellite $C$, et qui correspond le plus probablement à la position de la balise. En effet, il y a souvent deux points qui correspondent à ces critères. Dans ce cas, il faudra éliminer celui qui ne se trouve pas sur la Terre, puisque nous savons que le récepteur s'y trouve. Nous appelerons $R$ le point modélisant ce récepteur.
	
	L'objet géométrique s'imposant pour modéliser cette contrainte de distance est la sphère. En effet, par définition, il s'agit de l'ensemble de points à une distance fixe d'un centre, dans un repère tridimensionnel. Soient donc $S_A$, $S_B$ et $S_C$ trois sphères de centres respectifs $A$, $B$ et $C$, et de rayons respectifs $r_A$, $r_B$ et $r_C$ (\emph{cf} la figure~\ref{fig:equations1} pour visualiser ces hypothèses). Chacune de ces sphères est définie par une équation de sphère~:
	\begin{dgroup*}
		\begin{dmath*}S_A\colon (x - x_A)^2 + (y - y_A)^2 + (z - z_A)^2 \hiderel{=} r_A^2\end{dmath*}
		\begin{dmath*}S_B\colon (x - x_B)^2 + (y - y_B)^2 + (z - z_B)^2 \hiderel{=} r_B^2\end{dmath*}
		\begin{dmath*}S_C\colon (x - x_C)^2 + (y - y_C)^2 + (z - z_C)^2 \hiderel{=} r_C^2\end{dmath*}.
	\end{dgroup*}
\end{minipage}
\hspace{0.05\linewidth}
\fbox{\begin{minipage}[t]{0.45\linewidth}
	% apply indenting
	\parindent=\oldparindent
	
	\paragraph{\'{E}quations cartésiennes de cercles et de sphères}~\\
	
	Les équations cartésiennes de cercles s'apparentent aux équations cartésiennes de droites, dans le sens où elles sont vérifiées lorsqu'on substitue $x$ et $y$ par les coordonnées
	d'un point appartenant au cercle qu'elles déterminent. Dans un repère orthonormal, soient un cercle $C$ de centre $O$ et de rayon $r$, un point $A$ sur ce cercle et un point $H$
	intersection de la parallèle à $(Ox)$ passant par $O$ et de la parallèle à $(Oy)$ passant par $A$.
	
	Le triangle $AHO$ étant rectangle en $H$, on a la relation suivante, d'après le théorème de Pythagore~:
	\begin{dmath*}AH^2 + HO^2 = AO^2.\end{dmath*}
	
	Or, par définition on a $AO = r$. De plus, sachant que $AH=|y_A-y_H|$ et donc que $AH=|y_A-y_O|$, que $HO=|x_H-x_O|$ et donc que $HO=|x_A-x_O|$ (voir figure~\ref{fig:equations2}),
	on a~:
	\begin{dmath*}(x_A-x_O)^2 + (y_A-y_O)^2 = r^2.\end{dmath*}
	
	Il s'agit de l'équation cartésienne du cercle $C$. On a, de même, l'équation de la sphère $S$ de rayon $r$ et de centre $O$~:
	\begin{dmath*}(x - x_O)^2 + (y - y_O)^2 + (z - z_O)^2 = r^2.\end{dmath*}
\end{minipage}}
\parindent=\oldparindent

\begin{figure}
	\caption{Illustration des hypothèses}
	\label{fig:equations1}
	\begin{center}
		\includegraphics[width=15cm]{equations/fig1}
	\end{center}
\end{figure}

\subsection{Système de trois équations à trois inconnues}

Le point $R$ étant situé à l'intersection de ces trois sphères, ses coordonnées $x$, $y$ et $z$ vérifient les équations des trois sphères, c'est-à-dire qu'elles vérifient le système d'équations~\eqref{eq:sys1}~:\\
\Schema{0ex}{5ex}{}{
\begin{dgroup}[label={eq:sys1}]
	\begin{dmath*}(x - x_A)^2 + (y - y_A)^2 + (z - z_A)^2 = r_A^2\end{dmath*}
	\begin{dmath*}(x - x_B)^2 + (y - y_B)^2 + (z - z_B)^2 = r_B^2\end{dmath*}
	\begin{dmath*}(x - x_C)^2 + (y - y_C)^2 + (z - z_C)^2 = r_C^2\end{dmath*}.
\end{dgroup}
}

Dans ce système, $x_A, x_B, x_C, y_A, y_B, y_C, z_A, z_B, z_C, r_A, r_B$ et $r_C$ sont des paramètres connus. En effet, les coordonnées des satellites sont émises par les satellites eux-mêmes dans leur signaux. De plus, les rayons de sphères ont été déterminés précédemment. Il faut toutefois savoir que nous avons ici négligé pour des raisons de simplicité la synchronisation des horloges du satellite avec le récepteur, ce qui nécessiterait une quatrième inconnue et équation.

Ce système pourrait donc se résoudre mathématiquement puisque nous avons trois équations à trois inconnues~; cependant, comme nous allons le voir, les balises~---~dotées de microcontrôleurs souvent peu puissants~---~ne résolvent pas précisément cette équation de navigation mais approchent plutôt les solutions en utilisant la méthode de Newton-Raphson.