\section{Utilisation de la méthode de Newton-Raphson}

Un récepteur devant actualiser ses informations de position plusieurs fois par seconde, il est nécessaire d'effectuer les calculs d'intersection des sphères très rapidement. Cela n'est pas possible en résolvant de manière mathématique le système d'équations, ce qui recquiert un temps non-négligeable.

La solution adoptée par la plupart des fabriquants de récepteurs \textsc{Gps} est l'utilisation de la méthode de Newton-Raphson (ici abrégée en méthode de Newton)~\cite{wkpdNewton} pour approcher ces valeurs. La vitesse d'exécution de cet algorithme est bien plus acceptable, et ses résultats sont très proches de la réalité.

Nous présenterons tout d'abord le fonctionnement de la méthode, puis son application dans le cas de la résolution du système.

\subsection{Algorithme}

La méthode de Newton est prévue pour étudier les racines des fonctions à une variable, et donc à deux dimensions. Elle consiste à se rapprocher petit à petit de la valeur en suivant un algorithme récursif dont le nombre d'itérations détermine la précision.

Soient $f$ et $f\prime$ une fonction et sa dérivée, et $k$ un entier naturel. On cherche $x$ l'ascisse d'une des racines en utilisant la suite~\cite{mlnxNewton}~: \begin{dmath*}x_k = x_{k-1} - \frac{f(x_{k-1})}{f\prime(x_{k-1})}.\end{dmath*} La valeur initiale $x_0$ est choisie arbitrairement, et est souvent une estimation grossière de l'ascisse que l'on cherche à trouver.

\nocite{ohuvManmlo}
\nocite{muagNewapp}
\nocite{epflOplico}

\subsection{Exemple}

Illustrons ici la méthode avec la fonction polynôme du second degré \begin{dmath*}f\colon x \mapsto \sqrt{5}x^2 - 2x - 1,\end{dmath*} dont on cherche les racines. Sur la figure~\ref{fig:newton1}, on a tracé $f$ en rouge et $f\prime$ en vert. Nous donnons une première estimation de la racine, qui est de $x_0 = -0.5$, qui correspond au point $X_0$.

\begin{figure}
	\caption{Fonctions $f, f\prime$ et estimation grossière de la position d'une des racines}
	\label{fig:newton1}
	\begin{center}
		\includegraphics[width=15cm]{newton/fig1}
	\end{center}
\end{figure}

\begin{figure}
	\caption{Fonctions $f, f\prime$ et première approximation de la position d'une des racines}
	\label{fig:newton2}
	\begin{center}
		\includegraphics[width=15cm]{newton/fig2}
	\end{center}
\end{figure}

\begin{figure}
	\caption{Fonction $f$ et approximation précise de la position d'une des racines}
	\label{fig:newton3}
	\begin{center}
		\includegraphics[width=15cm]{newton/fig3}
	\end{center}
\end{figure}

ll est possible ensuite de calculer les valeurs de la suite. Ici, nous avons calculé les deux premières valeurs, mais, dans la réalité, il est nécessaire d'en calculer plus pour obtenir une précision adéquate.

\begin{dgroup*}
	\begin{dmath*}x_0 = -0.5\end{dmath*}
	\begin{dmath*}x_1 = x_0 - \frac{f(x_0)}{f'(x_0)}\end{dmath*}
	\begin{dmath*}x_1 \approx -0.5 - \frac{0.559}{4.2361}\end{dmath*}
	\begin{dmath*}x_1 \approx -0.368\end{dmath*}
	\begin{dmath*}x_2 = x_1 - \frac{f(x_1)}{f'(x_1)}\end{dmath*}
	\begin{dmath*}x_2 \approx -0.368 - \frac{0.0388}{-3.6457}\end{dmath*}
	\begin{dmath*}x_2 \approx -0.3574\end{dmath*}
\end{dgroup*}

Ces étapes de calculs sont illustrées en figures~\ref{fig:newton2}~et~\ref{fig:newton3} dans lesquelles on voit nettement que le point $X_2$ se rapproche du point de la racine exacte.

\subsection{Construction géométrique}

On illustre généralement cette méthode par sa construction géométrique, qui s'obtient en suivant l'algorithme suivant~:

\begin{enumerate}
\item tracer la perpendiculaire $(p_{k-1})$ de $(Ox)$ en $X_{k-1}$~;
\item placer le point $I_k$ intersection de $(p_{k-1})$ et $C_f$~;
\item tracer la tangeante $(t_k)$ à $C_f$ en $I_k$~;
\item placer le point $X_k$ intersection de $(t_k)$ et $(Ox)$.
\end{enumerate}

\begin{figure}
	\caption{Première itération de l'algorithme géométrique}
	\label{fig:newton4}
	\begin{center}
		\includegraphics[width=15cm]{newton/fig4}
	\end{center}
\end{figure}

\begin{figure}
	\caption{Seconde itération de l'algorithme géométrique}
	\label{fig:newton5}
	\begin{center}
		\includegraphics[width=15cm]{newton/fig5}
	\end{center}
\end{figure}

Les figures~\ref{fig:newton4}~et~\ref{fig:newton5} montrent les deux premières itérations de cet algorithme, appliqué à la fonction $f$ vue précédemment.

\subsection{Application sur l'intersection de 4 sphères}

Nous savons que l'équation de la sphère $S_n$~: \begin{dmath*}(x - x_n)^2 + (y - y_n)^2 + (z - z_n)^2 = r_n\end{dmath*} est vérifiée pour tout point $(x, y, z)$ sur cette sphère, d'où l'équation~: \begin{dmath*}(x - x_n)^2 + (y - y_n)^2 + (z - z_n)^2 - r_n = 0.\end{dmath*}

La fonction \begin{dmath*}f\colon (x, y, z) \mapsto (x - x_n)^2 + (y - y_n)^2 + (z - z_n)^2 - r_n,\end{dmath*} est donc telle que l'équation \begin{dmath*}f(x, y, z) = 0\end{dmath*} a pour solutions les coordonnées des points de $S_n$.

La méthode de Newton décrite précédemment nous permet précisément de trouver des approximations des points dont les coordonnées sont solution de l'équation précédente.

Il s'agit alors de trouver les solutions de cette fonction pour les $n$ sphères utilisées. L'intersection de ces solutions sera l'intersection des $n$ sphères. Cette fonction étant à trois variables, nous ne détaillerons pas ce point d'application de la méthode ici.